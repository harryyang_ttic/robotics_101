# This is a template for a Vitirbi class that can be used to compute
# most likely sequences.

class Viterbi(object):
    # Construct an instance of the viterbi class
    # based upon an instantiatin of the HMM class
    def __init__(self, hmm):
        self.hmm = hmm

        # Your code goes here


    # Returns the most likely state sequence for a given output
    # (observation) sequence, i.e.,
    #    arg max_{X_1, X_2, ..., X_T} P(X_1,...,X_T | Z_1,...Z_T)
    # according to the HMM model that was passed to the constructor.
    def mostLikelySequence(output):

        # Your code goes here
        print "Your code goes here"
